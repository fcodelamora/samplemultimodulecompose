package com.training.nasa.apod.debug.ui.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.training.nasa.apod.common.resources.ui.AppUserInterface
import com.training.nasa.apod.common.resources.ui.catalogs.views.Button_1
import com.training.nasa.apod.common.resources.ui.catalogs.views.Text_Information_1
import com.training.nasa.apod.debug.ui.catalogs.Text_SectionSubtitle
import com.training.nasa.apod.debug.ui.catalogs.Text_SectionTitle
import com.training.nasa.apod.debug.ui.catalogs.views.Spinner_1
import com.training.nasa.apod.debug.ui.catalogs.views.TextField_Numeric_1
import com.training.nasa.apod.debug.viewmodels.DebugViewModel

object DebugScreen {

    object Default {

        @Composable
        fun Screen(viewmodel: DebugViewModel) {

            Content(
                delayInMillis = viewmodel.delay.value,
                availableErrorList = viewmodel.availableErrorList,
                apiErrorTypeSelectedIndex = viewmodel.apiErrorTypeSelectionIndex.value,
                onDelayInMillisUpdated = { newValue -> viewmodel.updateDelayInMillis(newValue) },
                onApiErrorSelected = { index ->
                    viewmodel.apiErrorTypeSelectionIndex.value = index
                },
                onUpdateFlags = { viewmodel.updateFlags() }
            )
        }

        @Composable
        fun Content(
            delayInMillis: String = "250",
            availableErrorList: List<String> = listOf("No Error"),
            apiErrorTypeSelectedIndex: Int = 0,
            onDelayInMillisUpdated: (String) -> Unit = {},
            onApiErrorSelected: (Int) -> Unit = {},
            onUpdateFlags: () -> Unit = {}
        ) {

            val scrollState = rememberScrollState()

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .padding(8.dp, 4.dp, 8.dp, 4.dp)
                    .fillMaxWidth()
                    .verticalScroll(scrollState)
            ) {
                Text_Information_1(text = "PRESS BACK TO CANCEL")

                Text_SectionTitle(text = "API")
                Text_SectionSubtitle(text = "Errors")
                Spinner_1(
                    availableErrorList,
                    apiErrorTypeSelectedIndex,
                    onApiErrorSelected
                )

                Text_SectionSubtitle(text = "API Response Delay (ms)")
                TextField_Numeric_1(
                    value = delayInMillis.toString(),
                    onValueChange = onDelayInMillisUpdated
                )

                Button_1(buttonText = "SET FLAGS") { onUpdateFlags.invoke() }
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun DebugScreenPreview() {
    AppUserInterface {
        DebugScreen.Default.Content()
    }
}
