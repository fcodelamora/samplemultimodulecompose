package com.training.nasa.apod

import android.app.Application
import coil.ImageLoader
import coil.ImageLoaderFactory
import coil.util.CoilUtils
import dagger.hilt.android.HiltAndroidApp
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import timber.log.LogcatTree
import timber.log.Timber
import timber.log.Tree

@HiltAndroidApp
open class NasaApodApplication : Application(), ImageLoaderFactory {

    override fun onCreate() {
        super.onCreate()

        // Setup Libraries
        setupTimber()
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(SingleTagTree())
        }
    }

    private class SingleTagTree : Tree() {

        private val baseTree = LogcatTree("NasaAPOD")

        override fun isLoggable(priority: Int, tag: String?): Boolean {
            return BuildConfig.DEBUG
        }

        override fun performLog(
            priority: Int,
            tag: String?,
            throwable: Throwable?,
            message: String?
        ) {
            baseTree.log(priority, tag, throwable, message)
        }
    }

    // Set maxRequest to prevent connection issues to the image server due to overload
    override fun newImageLoader(): ImageLoader {

        val limitedRequestsDispatcher = Dispatcher().apply { maxRequests = 3 }

        return ImageLoader.Builder(applicationContext)
            .crossfade(true)
            .okHttpClient {
                OkHttpClient.Builder()
                    .dispatcher(limitedRequestsDispatcher)
                    .cache(CoilUtils.createDefaultCache(applicationContext))
                    .build()
            }
            .build()
    }
}
