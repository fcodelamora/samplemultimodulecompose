package commons

import BuildModules

plugins {
    id("commons.android-library")
}

dependencies {
    implementation(project(BuildModules.Core.USECASES))
    implementation(project(BuildModules.Core.ENTITIES))
}
