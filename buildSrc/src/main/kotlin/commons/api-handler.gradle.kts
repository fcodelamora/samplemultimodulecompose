package commons

import AndroidBuildConfig
import BuildModules

plugins {
    id("commons.android-library-base")
}

android {
    val baseUrlKey = "API_BASE_URL"
    val baseUrlValue = "\"https://api.nasa.gov/planetary/\""


    flavorDimensions += setOf(AndroidBuildConfig.ProductDimensions.ENVIRONMENT)

    productFlavors {
        create("product") {
            dimension = AndroidBuildConfig.ProductDimensions.ENVIRONMENT
            buildConfigField("String", baseUrlKey, baseUrlValue)
        }
        create("qa") {
            dimension = AndroidBuildConfig.ProductDimensions.ENVIRONMENT
            buildConfigField("String", baseUrlKey, baseUrlValue)
        }
        create("dev") {
            dimension = AndroidBuildConfig.ProductDimensions.ENVIRONMENT
            buildConfigField("String", baseUrlKey, baseUrlValue)
        }
    }
}

dependencies {
    implementation(project(BuildModules.Core.APIS))
}