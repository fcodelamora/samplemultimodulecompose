package commons

plugins {
    id("commons.android-library-base")
}

dependencies {
    implementation(project(BuildModules.Common.RESOURCES))
    implementation(project(BuildModules.Common.DEPENDENCY_INJECTION))
}