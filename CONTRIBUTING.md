This project code is formatted in 2 steps to facilitate `diff` reviews as well as a consistent
format across environments.

### Android Studio

Macro to auto-format each time "Save all" is triggered.

##### Preparation steps

1) In Android Studio, select Code -> Optimize Imports. Click on "Do not show this message again" and
   run it.

##### Creating the Macro

1) Edit -> Macros -> Start Recording Macro
2) Code -> Optimize Imports
3) Code -> Reformat Code
4) File -> Save All
5) Edit -> Macros -> Stop Macro Recording. Save the Macro. (Ex. ReformatCode)
6) Android Studio -> Settings / Preferences -> Keymap
7) Main Menu -> Edit -> *Select your macro from the Macros folder*, Add Shortcut
8) Use the same shortcut as "Save All" (Ex. Ctrl + S, ⌘ + S )
9) Remove the shortcut from "Save All". Your Macro should be your new "Save All" trigger

### ktlint

A pre-commit hook file is provided in the root of the project. Enable it using below commands in the
project root folder. (Tested on Ubuntu 20.04 LTS)

* `cp pre-commit-ktlint .git/hooks/pre-commit`
* `chmod 755 .git/hooks/pre-commit`

Below Tasks are executed to check and format code:

*`./gradlew ktlintCheck`
*`./gradlew --continue ktlintFormat`
