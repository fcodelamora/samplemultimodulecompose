package com.training.nasa.apod.common.resources.ui.catalogs.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.statusBarsHeight
import com.training.nasa.apod.common.resources.ui.AppDrawable
import com.training.nasa.apod.common.resources.ui.catalogs.CatalogView

@Composable
fun TopAppBar_1(
    modifier: Modifier = Modifier,
    title: String = "",
    backgroundColor: Color = MaterialTheme.colors.primary,
    contentsColor: Color = MaterialTheme.colors.onPrimary,
    onNavigationIconClick: (() -> Unit)? = {}
) {
    Column(modifier) {
        Surface(
            color = backgroundColor,
            content = {},
            modifier = modifier
                .statusBarsHeight()
                .fillMaxWidth()
        )
        TopAppBar(
            backgroundColor = backgroundColor,
            title = {
                Text(
                    text = title,
                    color = contentsColor,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            },
            elevation = 0.dp,
            navigationIcon = {
                IconButton(onClick = { onNavigationIconClick?.invoke() }) {
                    Icon(
                        painter = painterResource(id = AppDrawable.ic_arrow_back),
                        tint = contentsColor,
                        contentDescription = "Back"
                    )
                }
            }
        )
    }
}

@Composable
fun MarginDecorator(
    modifier: Modifier
) {
    Box(
        modifier
            .statusBarsHeight(50.dp)
            .background(
                brush = Brush.verticalGradient(
                    listOf(
                        Color.Black.copy(alpha = 0.75f),
                        Color.Transparent
                    )
                )
            )
    )
}

@Composable
fun TopAppBar_2(
    modifier: Modifier = Modifier,
    onNavigationIconClick: (() -> Unit)? = {}
) = TopAppBar_1(
    onNavigationIconClick = onNavigationIconClick,
    // Get a White arrow from TopBar
    backgroundColor = Color.Transparent,
    contentsColor = MaterialTheme.colors.secondaryVariant,
    modifier = modifier
)

@Preview
@Composable
fun TopAppBarCatalogPreview() {
    CatalogView {
        TopAppBar_1(title = "TopAppBar_1")
        Divider_1()
        TopAppBar_2()
    }
}
