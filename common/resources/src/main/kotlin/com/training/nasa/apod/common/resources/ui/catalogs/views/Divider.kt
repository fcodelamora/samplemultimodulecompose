package com.training.nasa.apod.common.resources.ui.catalogs.views

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.training.nasa.apod.common.resources.ui.catalogs.CatalogView
import com.training.nasa.apod.common.resources.ui.catalogs.DummyText

// Divider Catalog

@Composable
fun Divider_1(modifier: Modifier = Modifier) = Divider(
    color = Color.Black,
    modifier = modifier.padding(
        PaddingValues(horizontal = 8.dp, vertical = 8.dp)
    )
)

@Composable
fun Divider_2(modifier: Modifier = Modifier) = Divider(
    color = MaterialTheme.colors.primary,
    modifier = modifier.padding(
        PaddingValues(horizontal = 8.dp, vertical = 4.dp)
    )
)

@Composable
fun Divider_3(modifier: Modifier = Modifier) = Divider(
    color = Color.Blue,
    modifier = modifier.padding(
        PaddingValues(horizontal = 8.dp, vertical = 2.dp)
    )
)

@Composable
fun Divider_4(modifier: Modifier = Modifier) = Divider(
    color = MaterialTheme.colors.secondary,
    modifier = modifier
)

@Preview(showSystemUi = true)
@Composable
fun DividerCatalogPreview() {
    CatalogView {
        DummyText()
        Divider_1()
        DummyText()
        Divider_2()
        DummyText()
        Divider_3()
        DummyText()
        Divider_4()
    }
}
