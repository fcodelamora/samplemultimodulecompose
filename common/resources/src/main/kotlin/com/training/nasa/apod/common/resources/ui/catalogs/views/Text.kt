package com.training.nasa.apod.common.resources.ui.catalogs.views

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.training.nasa.apod.common.resources.ui.catalogs.CatalogView

// Compose Text

@Composable
fun Text_Title_1(
    modifier: Modifier = Modifier,
    text: String = ""
) {
    Text(
        text = text,
        color = MaterialTheme.colors.secondary.copy(alpha = 0.9f),
        style = MaterialTheme.typography.h4,
        textAlign = TextAlign.Center,
        modifier = modifier,
    )
}

@Composable
fun Text_Title_2(
    modifier: Modifier = Modifier,
    text: String = ""
) {
    Text(
        text = text,
        color = MaterialTheme.colors.secondary.copy(alpha = 0.9f),
        style = MaterialTheme.typography.h5,
        textAlign = TextAlign.Center,
        modifier = modifier,
    )
}

@Composable
fun Text_Caption_1(
    modifier: Modifier = Modifier,
    text: String = ""
) = Text(
    text = text,
    fontSize = 15.sp,
    color = MaterialTheme.colors.contentColorFor(
        MaterialTheme.colors.background
    ).copy(alpha = 0.7f),
    style = MaterialTheme.typography.caption.copy(
        fontWeight = FontWeight.Bold,
        letterSpacing = 2.0.sp
    ),
    textAlign = TextAlign.Justify,
    modifier = modifier,
)

@Composable
fun Text_Contents_2(
    modifier: Modifier = Modifier,
    text: String = ""
) = Text(
    text = text,
    style = MaterialTheme.typography.body2,
    textAlign = TextAlign.Justify,
    modifier = modifier,
)

@Composable
fun Text_Information_1(
    modifier: Modifier = Modifier,
    text: String = ""
) = Text(
    text = "ⓘ  $text",
    color = MaterialTheme.colors.secondary,
    fontSize = 10.sp,
    textAlign = TextAlign.Center,
    modifier = modifier
        .fillMaxWidth()
        .padding(20.dp),
)

@Composable
fun Text_Information_2(
    modifier: Modifier = Modifier,
    text: String = ""
) = Text(
    text = text,
    color = MaterialTheme.colors.secondary.copy(alpha = 0.5f),
    fontSize = 10.sp,
    textAlign = TextAlign.Center,
    modifier = modifier
)

@Composable
@Preview(showSystemUi = true)
fun TextCatalogPreview() {
    CatalogView {
        Text_Title_1(text = "Text_Title_1")
        Divider_1()
        Text_Caption_1(text = "Text Caption 1")
        Divider_1()
        Text_Contents_2(text = "Text_Contents_2")
        Divider_1()
        Text_Information_1(text = "Sample Text")
    }
}
