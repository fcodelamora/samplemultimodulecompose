package com.training.nasa.apod.common.resources.ui.catalogs.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)

val Teal200 = Color(0xFF03DAC5)
val Teal700 = Color(0xFF018786)

val Gray100 = Color(0xFFF5F5F5)
val Gray200 = Color(0xFFEEEEEE)
val Gray400 = Color(0xFFBDBDBD)
val Gray700 = Color(0xFF616161)
val Gray900 = Color(0xFF212121)

val Red900 = Color(0xFFB30000)
val Green900 = Color(0xFF005F00)
