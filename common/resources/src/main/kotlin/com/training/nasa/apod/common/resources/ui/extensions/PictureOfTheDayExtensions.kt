package com.training.nasa.apod.common.resources.ui.extensions

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import com.training.nasa.apod.core.entities.PictureOfTheDay

fun PictureOfTheDay.getThumbnailColorFilter(tintColor: Color) = if (thumbnail == null)
    ColorFilter.tint(tintColor.copy(alpha = 0.8f))
else
    null
