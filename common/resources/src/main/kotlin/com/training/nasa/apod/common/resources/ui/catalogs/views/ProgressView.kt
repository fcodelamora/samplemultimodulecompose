package com.training.nasa.apod.common.resources.ui.catalogs.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.training.nasa.apod.common.resources.ui.catalogs.CatalogView

// ProgressViews

@Composable
fun ProgressView_1() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background.copy(alpha = 0.7f))
            .clickable(enabled = false) {}
    ) {
        CircularProgressIndicator(
            color = MaterialTheme.colors.secondary,
            modifier = Modifier
                .size(75.dp)
                .align(Alignment.Center)
        )
    }
}

@Composable
@Preview(showSystemUi = true)
private fun ProgressViewCatalogPreview() {
    CatalogView {
        ProgressView_1()
    }
}
